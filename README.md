# Shaped Maze Generator

```
              ┌─┐                        
              │ │                        
      ┌─┐     │ └─┬─┐                    
      │ │     │   │ │                    
    ┌─┤ └───┬─┘ │ │ └─┐                  
    │ │     │   │     │                  
    │ │ ┌─┐ │ │ └─┬─┐ │                  
    │   │ │   │   │ │ │                  
┌───┘ ──┤ └───┴─┐ │ │ ├───┐              
│       │       │ │ │ │   │              
└───┬─┐ │ │ │ ┌─┘ │ │ │ ┌─┴───┐          
    │ │ │ │ │ │   │     │     │          
    │ │ └─┤ ├─┘ ┌─┘ ┌───┘ │ ──┤          
    │ │   │ │   │   │     │   │          
    │ └─┐ │ │ ──┴───┘ ┌───┴─┐ └─┐        
    │   │ │ │         │     │   │        
  ┌─┘ ──┤ │ └─────────┼── ┌─┴─┐ └───┐    
  │     │ │           │   │   │     │    
  └─┐ │ │ │ ──┬─┐ ┌── │ ──┘ │ └─┬─┐ └─┐  
    │ │   │   │ │ │   │     │   │ │   │  
    └─┴─┐ └─┐ │ │ │ ──┤ ┌───┘ │ │ │ ┌─┘  
        │   │   │ │   │ │     │   │ │    
        └─┐ └─┐ │ └─┐ └─┘ ┌───┴───┘ │    
          │   │ │   │     │         │    
          ├── └─┤ │ │ ┌───┤ ──┬───┐ │    
          │     │ │ │ │   │   │   │ │    
          └───┐ └─┘ │ │ │ │ │ │ │ │ └───┐
              │     │ │ │ │ │ │ │ │     │
              └───┬─┘ │ │ └─┤ └─┤ ├── ┌─┘
                  │   │ │   │   │ │   │  
                  │ ──┤ ├── └── │ │ ──┤  
                  │   │ │       │     │  
                  └─┐ │ └─┬─────┤ ──┬─┘  
                    │ │   │     │   │    
                    │ └─┐ │     └───┘    
                     ^  │ │              
                     ■  │ │              
                         ■               
                         v               
```

I made this in summer of 2015, when I was learning C++!
It was my first maze generator!

It is a very biased maze generator and it creates pretty patterns!

Instead of generating randomly, this generator uses a finite-state machine to choose directions!
The rules of the state-machine are determined randomly, depending on the seed!

The size of the area in which the maze generates is also determined by the seed!
Smaller sizes are more likely than bigger ones, but it is also possible to get really huge ones!

For positioning the entrance and exit, it uses a brute force algorithm and it sometimes takes a long time.
Please forgive the past me, I didn't know what I was doing back then!

## Compile:

```
git clone https://gitlab.com/MRAAGH/shaped-maze-generator
cd shaped-maze-generator
make
```

## Run:

`./smgx`
