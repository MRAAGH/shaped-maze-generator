#include<iostream>
#include<cstdlib>
#include<cmath>
#include<vector>
#include<string>
#include<sstream>
#include<fstream>
using namespace std;
string croprintright(vector<vector<vector<bool> > >&walls,string symb[]);
string croprintright(vector<vector<vector<bool> > >&walls,int arrows[2][3],string symb[],string harr[]);
int crop0(vector<vector<vector<bool> > >&walls);
int crop1(vector<vector<vector<bool> > >&walls);
int crop2(vector<vector<vector<bool> > >&walls);
int crop3(vector<vector<vector<bool> > >&walls);
bool verwall(int x,int y,vector<vector<vector<bool> > >&walls);
bool horwall(int x,int y,vector<vector<vector<bool> > >&walls);
int intersect(int x,int y,vector<vector<vector<bool> > >&walls);
string midharr(int x,int y,int arrows[2][3],string symb[],string harr[]);
string verharr(int x,int y,vector<vector<vector<bool> > >&walls,int arrows[2][3],string symb[],string harr[]);
string horharr(int x,int y,vector<vector<vector<bool> > >&walls,int arrows[2][3],string symb[],string harr[]);
string interharr(int x,int y,vector<vector<vector<bool> > >&walls,string symb[]);
bool cwall(int x,int y,int d,vector<vector<vector<bool> > >&walls);
bool c(int x,int y,vector<vector<int> >&map,int searched);
bool cb(int x,int y,vector<vector<int> >&map,int searched);
bool cs(int x,int y,int d,vector<vector<int> >&map);
int cycle(int&value,int&state,vector<vector<vector<int> > >&cards);
int mx(int x,int d);
int my(int y,int d);
int mx(int x,int d,int n);
int my(int y,int d,int n);
void mxx(int&x,int d);
void myy(int&y,int d);
void mxy(int&x,int&y,int d);
int cornx(int d,int MW);
int corny(int d,int MH);
int ccw(int d);
int cw(int d);
int rvs(int d);
void ccw_(int&d);
void cw_(int&d);
void rvs_(int&d);
int edgx(int e,int MH,int MW);
int edgy(int e,int MH,int MW);
int edgd(int e,int MH,int MW);
int edgccw(int e,int MH,int MW);
int edgcw(int e,int MH,int MW);
int convert(string x);
int myhash(string x);
int main(){


	/*for(int i=0;i<4;i++){
		int x=0,y=0;
		mxy(&x,&y,i);
		cout<<"d "<<i<<" x "<<x<<" y "<<y<<"\n";
	}
	return 0;
*/


	string symbold[]={" ","-","|","+","-","-","+","+","|","+","|","+","+","+","+","+"};
	string symbnew[]={" ","─","│","└","─","─","┘","┴","│","┌","│","├","┐","┬","┤","┼"};
	string harrold[]={">","^","<","v","-","|","-","|","-","|","-","|","<","v",">","^"};
	string harrnew[]={">","^","<","v","■","■","■","■","■","■","■","■","<","v",">","^"};
	string prevseed="";
	bool notfirst=false;
	cout<<"2x same seed => entr & exit + txt file (takes a while).\n\n";
    //Main loop:
    while(true){
        //Accept the seed:
    	string seed;
		cout<<"Seed: ";
		getline(cin,seed);
        //Seed the main random number generator:
        srand(convert(seed));
        //Generate the generation field size (the border beyond which the maze can't be generated):
        //The old version was just rand()%30000 but I'm afraid this strongly prefers lower values because 30000 is so close to max int.
        int sizebase=rand()%300*100+rand()%100;
        //A function which causes by far most sizes to be between 50 and 90 while the true min is 5 and max 1000:
        int size=pow(1.006822,sizebase-29000)-pow(1.3,-sizebase+14.5)+0.002*sizebase+50;
        //Maze height and maze width:
        int MH=size,MW=size;
        //Generate the amount of cards:
        int cardbase=rand()%30;
        //A function which causes most card counts to be between 6 and 9 while the true min is 1 and max 75:
        int CARDS=pow(2,cardbase-23)-pow(2,-cardbase+2)+0.2*cardbase+5;
        //Generate generators for shape and path generation:
        //Each one has a current value and the next card ID:
        int value[4],state[4];
        //See the cards manual at the end of this file.
        vector<vector<vector<vector<int> > > >cards(4,vector<vector<vector<int> > >(CARDS,vector<vector<int> >(2,vector<int>(3,0))));
        //Initialize or generate:
        for(int i=0;i<4;i++){
        	//Each generator starts at value 0 and state 0:
        	value[i]=0;
        	state[i]=0;
        	//The cards are randomly generated:
        	for(int card=0;card<CARDS;card++)
        		//Generate "set" and "value" values (generators of index 0 and 2 only have two of these because they're double generators):
        		for(int j=0;j<2+i%2;j++){
        			//Generate "set" (since they're also pointers to themselves we need to use 2+i%2 again):
                    cards[i][card][0][j]=rand()%(2+i%2);
                    //Generate "value":
                    cards[i][card][1][j]=rand()%CARDS;
        		}
        }
        //Create field (two representations, one for walls and one for squares):
        //See the manual at the end of this file.
        vector<vector<vector<bool> > >walls(MH,vector<vector<bool> >(MW,vector <bool>(4,false)));
        vector<vector<int> >map(MH,vector<int>(MW));
        //Choose the starting point for shape generation:
        int xstart=rand()%MW,ystart=rand()%MH,x=xstart,y=ystart;
        //dir is used to keep track of where we came from while generating.
        int dir=0;
        //index keeps track of the exact path of the generator and makes backtracking possible.
        int index=0;
        //Shape generation loop:
        while(true){
        	//Find available directions to continue the border:
            bool dirs[4]={};
            int count=0;
            int d;
            for(int d2=0;d2<4;d2++)
                if(cs(mx(x,d2),my(y,d2),d2,map)){
                    dirs[d2]=true;
                    count++;
                    d=d2;
                }
            //Decide what to do based on how many were found:
			if(count>0){
				//Mark this spot as border:
				index++;
				map[y][x]=index;
				//Expand the border.
				//Choose the direction to expand in:
				if(count==4)
					//Only at the very beginning are there four available directions.
					//It is okay to use the general randomizer in this case.
					d=rand()%4;
				else{
					//Use turing machines if required:
					int chosen=1;
					if(count==2)chosen=cycle(value[0],state[0],cards[0])+1;
					if(count==3)chosen=cycle(value[1],state[1],cards[1])+1;
					//The following finds out which of the available
					//directions is chosen-th from the right.
					d=cw(dir);
					while(chosen){
						//Check if this one is ok:
						chosen-=dirs[d];
						//Rotate ccw:
						if(chosen)ccw_(d);
					}
				}
				//Update direction:
				dir=d;
			}
			else{
				//Abandon this border:
				map[y][x]=-1;
				//Backtrack if possible, otherwise error.
				//Find a valid direction to move in
				//(it needs to be the of the previous index):
				d=3;
				while(!c(mx(x,d),my(y,d),map,index)){
					d--;
					//If there is no such direction, error because that's not supposed to happen.
					if(d<0){
						cout<<"Shape generation has failed for seed \""<<seed<<"\"!\n";
						string pause;
						cin>>pause;
					}
				}
				index--;
			}
			//A direction should be found by now.
			//Move there:
			mxy(x,y,d);
			//If near the beginning, loop back to it and finish.
			//We need to be exactly 2 spaces away and there needs to be emptiness between.
			//There also needs to be space to the left and the right or this could look weird.
			//Check all directions:
			for(int dirc=0;dirc<4;dirc++)
				//Check if the start is 2 spaces in this direction:
				if(mx(x,dirc,2)==xstart&&my(y,dirc,2)==ystart)
					//Check if there's an empty space between:
					if(c(mx(x,dirc),my(y,dirc),map,0)){
						map[y][x]=index+1;
						map[my(y,dirc)][mx(x,dirc)]=index+2;
						goto shapefin;
					}
        }
        shapefin:
		//The map now has a border drawn in it, with values above 0.
		//The rest is full of zeroes and minus ones.
		//The next step is to change borders to 4 and everything else to 0.
		//Cleanup job:
		//NOTE: (y=0;y<MH;y++) is from bottom to top!
		for(y=0;y<MH;y++)
			for(x=0;x<MW;x++){
				if(map[y][x]>0)map[y][x]=4;
				else map[y][x]=0;
			}
		//Find a spot that is inside the maze (crucial to further generation):
		index=0;
		//Searching bottom-to-top(left-to-right):
		for(int by=0;by<MH;by++)
			for(int bx=0;bx<MW;bx++)
				if(map[by][bx]==4){
					//Found a corner!
					//One space up and to the right is within the maze!
					ystart=by+1;
					xstart=bx+1;
					goto srcfin;
				}
		srcfin:
		//Starting point:
		y=ystart;
		x=xstart;
		for(int d2=0;d2<4;d2++)
			walls[y][x][d2]=true;
        //dir is used to keep track of where we came from while generating.
        dir=0;
        //Path generation loop:
        while(true){
            //Find available directions:
            bool dirs[4]={};
            int count=0;
            int d;
            for(int d2=0;d2<4;d2++)
                if(c(mx(x,d2),my(y,d2),map,0)){
                    dirs[d2]=true;
                    count++;
                    d=d2;
                }
            //Decide what to do based on how many were found:
            if(count>0){
            	//Mark this spot as visited:
                map[y][x]=1;
                //Expand the maze.
                //Choose the direction to expand in:
                if(count==4)
                    //Only at the very beginning are there four available directions.
                    //It is okay to use the general randomizer in this case.
                    d=rand()%4;
                else{
                    //Use turing machines if required:
                    int chosen=1;
					if(count==2)chosen=cycle(value[2],state[2],cards[2])+1;
					if(count==3)chosen=cycle(value[3],state[3],cards[3])+1;
                    //The following finds out which of the available
                    //directions is chosen-th from the right.
                    d=cw(dir);
                    while(chosen){
                        //Check if this one is ok:
                        chosen-=dirs[d];
                        //Rotate ccw:
                        if(chosen)ccw_(d);
                    }
                }
                //Update direction:
                dir=d;
                //Drill a hole in this direction:
                walls[y][x][dir]=false;
                //Place walls around the next spot, except for the direction we are just coming from:
                for(int d2=0;d2<4;d2++)
                	if(d2!=rvs(dir))
                		walls[my(y,dir)][mx(x,dir)][d2]=true;
            }
            else{
                map[y][x]=2;
                //Backtrack or finish.
                //Find a valid direction to move in
                //(it needs to be of value 1 and there needs to be a hole):
                dir=3;



//CAUTION! CHECKING AN INVALID DIRECTION (4)! (hotfixed)

                while(!c(mx(x,dir),my(y,dir),map,1)||walls[y][x][dir]){
                    dir--;
                    //If there is no such direction, quit maze generation because we are finished with it.
                    if(dir<0)goto pathfin;
                }
            }
            //A direction should be found by now.
            //Move there:
            mxy(x,y,dir);
        }
        pathfin:
		//Did the user request entrance and exit?
		if(notfirst&&seed.compare(prevseed)==0){
			//Entrance and exit generation:
			//First, find all likely entrance positions.
			//Prepare vectors for the data about the already found positions which is used
			//to avoid duplicates, but it's also what is used for further e&e generation:
			vector<int>xent,yent,dent;
			//the xstart and ystart are still containing a position within the maze, namely at the bottom left.
			x=xstart;
			y=ystart;
			dir=0;
			//Go around the whole maze edge:
			do{




			}while(x!=xstart||y!=ystart);




			//Go along the edge of the maze:












			for(int scout=0;scout<2*MH+2*MW;scout++){
				//Convert to xyd:
				x=edgx(scout,MH,MW);
				y=edgy(scout,MH,MW);
				dir=edgd(scout,MH,MW);
				//Check if an entrance can be put here:
				//Travel until we encounter a border or get out of the map:
				while(c(x,y,map,0))
					mxy(x,y,dir);
				//Travel past the (supposed) maze border:
				while(c(x,y,map,4))
					mxy(x,y,dir);
				//If the next spot is 2, we found a valid entrance point.
				//If it's not, we're either outside the map or this is a
				//loopback border an entrance has nothing to do at.
				//If it's a valid border ...
				if(c(x,y,map,2)){
/*TEMP
					int tahh[2][3]={
						{
							//Entrance arrow:
							my(y,rvs(dir)),
							mx(x,rvs(dir)),
							dir
						},
						{
							//Exit arrow:
							my(y,rvs(dir)),
							mx(x,rvs(dir)),
							dir
						}
					};
					*/
					bool nondupe=true;
					//... also check if it's on the same square as one we've found before.
					for(int i=0;i<xent.size();i++)
						if(x==xent[i]&&y==yent[i]){
							nondupe=false;
							break;
						}
					//Add this spot to the list if it's not a duplicate:
					if(nondupe){
						xent.push_back(x);
						yent.push_back(y);
						dent.push_back(dir);
					}
				}
			}
			//Now, the actual e&e generation:
			int bestentr=0,bestexit=0,bestsolution=0,ref=3;
			//Cycle entrances:
			for(int entr=0;entr<xent.size();entr++){
				//Do not try every exit because that would be wasteful.
				//The algorithm can be sped up two times just by changing this.
				for(int exit=entr+1;exit<xent.size();exit++){
/*TEMP
					int ta[2][3]={
						{
							//Entrance arrow:
							my(yent[entr],rvs(dent[entr])),
							mx(xent[entr],rvs(dent[entr])),
							dent[entr]
						},
						{
							//Exit arrow:
							my(yent[exit],rvs(dent[exit])),
							mx(xent[exit],rvs(dent[exit])),
							dent[exit]
						}
					};
					cout<<"entr "<<ta[0][0]<<" "<<ta[0][1]<<" "<<ta[0][2]<<" exit "<<ta[1][0]<<" "<<ta[1][1]<<" "<<ta[1][2]<<"\n";
					cout<<croprintright(walls,ta,symbold,harrold);
*/
					//For this full set of arrow data, solve the maze.
					//This used to be in a function but since it gets executed about 80000 times (max 16000000)
					//I moved it here as one of my countless attempts of making it faster.
					int solution=0;
					x=xent[entr];
					y=yent[entr];
					dir=dent[entr];
					//Travel the maze until it's solved:
					//(it's solved when we're at the exit arrow)
					while(!(y==yent[exit]&&x==xent[exit])){
						//Check if we've been here already:
						if(map[y][x]==ref)solution-=2;
						//Mark this spot as visited:
						else map[y][x]=ref;
						//It follows the right hand edge.
						//Try to go right:
						cw_(dir);
						if(walls[y][x][dir]){
							//Try to go straight:
							ccw_(dir);
							if(walls[y][x][dir]){
								//Try to go left:
								ccw_(dir);
								if(walls[y][x][dir])
									//Dead end. Turn around and cry (no more checks, to improve performance):
									ccw_(dir);
							}
						}
						solution++;
						//A direction should be found by now.
						//Move there:
//cout<<"Moving from "<<y<<" "<<x<<" to "<<my(y,dir)<<" "<<mx(x,dir)<<"(dir "<<dir<<")\n"<<flush;
						mxy(x,y,dir);
					}
					//Is this the most complicated configuration so far?
					if(solution>bestsolution){
						//Update the longest solution:
						bestsolution=solution;
						bestentr=entr;
						bestexit=exit;
					}
					//This ref allows a very dirty way to work with the map array,
					//but it is also a very efficient one.
					ref++;
				}
			}
			//We are left with the best possible entrance and exit. Let's place them already:
			walls[yent[bestentr]][xent[bestentr]][rvs(dent[bestentr])]=false;
			walls[yent[bestexit]][xent[bestexit]][rvs(dent[bestexit])]=false;
			//Convert to arrows:
			int bestarrows[2][3]={
				{
					//Entrance arrow:
					my(yent[bestentr],rvs(dent[bestentr])),
					mx(xent[bestentr],rvs(dent[bestentr])),
					dent[bestentr]
				},
				{
					//Exit arrow:
					my(yent[bestexit],rvs(dent[bestexit])),
					mx(xent[bestexit],rvs(dent[bestexit])),
					dent[bestexit]
				}
			};
			//Crop and print:
			cout<<croprintright(walls,bestarrows,symbold,harrold);
			//Figure out the perfect font size:
			//int fontsize=maxfontsize;








			//Construct the filename:
			stringstream conv;
			conv<<seed<<".txt";
			string ofname;
			conv>>ofname;
			//Crop and save to file:
			ofstream sout(ofname.c_str());
			sout<<croprintright(walls,bestarrows,symbnew,harrnew);
			sout.close();
		}
		else
			//No entrance or exit
			//No saving to file
			//Just crop and print:
			cout<<croprintright(walls,symbold);
		notfirst=true;
		prevseed=seed;
    }
    //This is never reached because the application runs until it's terminated by the user.
    //Still,
    return 0;
}
//Crop and return the maze as text (no arrows):
string croprintright(vector<vector<vector<bool> > >&walls,string symb[]){
	//Crop:
	int xmax=crop0(walls),ymax=crop1(walls),xmin=crop2(walls),ymin=crop3(walls);
	//Convert to text:
	stringstream out;
	for(int y=ymax;y>=ymin;y--){
		//Print maze one row:
		//Horizontal wall with intersections:
		for(int x=xmin;x<=xmax;x++)out<<symb[intersect(x,y+1,walls)]<<symb[horwall(x,y+1,walls)];
		//Last intersection of the row:
		out<<symb[intersect(xmax+1,y+1,walls)]<<"\n";
		//Vertical walls with spaces between:
		for(int x=xmin;x<=xmax;x++)out<<symb[verwall(x,y,walls)*2]<<symb[0];
		//Last vertical wall of the row:
		out<<symb[verwall(xmax+1,y,walls)*2]<<"\n";
	}
	//Last row of intersections and horizontal walls:
	for(int x=xmin;x<=xmax;x++)out<<symb[intersect(x,ymin,walls)]<<symb[horwall(x,ymin,walls)];
	//Last intersection of the last row:
	out<<symb[intersect(xmax+1,ymin,walls)]<<"\n";
	return out.str();
}
//Crop and return the maze as text:
string croprintright(vector<vector<vector<bool> > >&walls,int arrows[2][3],string symb[],string harr[]){
	//Crop:
	int xmax=crop0(walls),ymax=crop1(walls),xmin=crop2(walls),ymin=crop3(walls);
	//Append optional empty space for arrows to fit in:
	xmax+=arrows[0][1]>xmax||arrows[1][1]>xmax;
	ymax+=arrows[0][0]>ymax||arrows[1][0]>ymax;
	xmin-=arrows[0][1]<xmin||arrows[1][1]<xmin;
	ymin-=arrows[0][0]<ymin||arrows[1][0]<ymin;
	//Convert to text:
	stringstream out;
	for(int y=ymax;y>=ymin;y--){
		//Print maze one row:
		//Horizontal wall with intersections:
		for(int x=xmin;x<=xmax;x++)out<<interharr(x,y+1,walls,symb)<<horharr(x,y+1,walls,arrows,symb,harr);
		//Last intersection of the row:
		out<<interharr(xmax+1,y+1,walls,symb)<<"\n";
		//Vertical walls with spaces between:
		for(int x=xmin;x<=xmax;x++)out<<verharr(x,y,walls,arrows,symb,harr)<<midharr(x,y,arrows,symb,harr);
		//Last vertical wall of the row:
		out<<verharr(xmax+1,y,walls,arrows,symb,harr)<<"\n";
	}
	//Last row of intersections and horizontal walls:
	for(int x=xmin;x<=xmax;x++)out<<interharr(x,ymin,walls,symb)<<horharr(x,ymin,walls,arrows,symb,harr);
	//Last intersection of the last row:
	out<<interharr(xmax+1,ymin,walls,symb)<<"\n";
	return out.str();
}
//Crop (or rather find the closest part of the maze to a specific field edge):
int crop0(vector<vector<vector<bool> > >&walls){
	for(int x=walls[0].size()-1;x>=0;x--)
		for(int y=0;y<walls.size();y++)
			for(int d=0;d<4;d++)
				if(walls[y][x][d])
					return x;
	return 0;
}
int crop1(vector<vector<vector<bool> > >&walls){
	for(int y=walls.size()-1;y>0;y--)
		for(int x=0;x<walls[0].size();x++)
			for(int d=0;d<4;d++)
				if(walls[y][x][d])
					return y;
	return 0;
}
int crop2(vector<vector<vector<bool> > >&walls){
	for(int x=0;x<walls[0].size();x++)
		for(int y=0;y<walls.size();y++)
			for(int d=0;d<4;d++)
				if(walls[y][x][d])
					return x;
	return 0;
}
int crop3(vector<vector<vector<bool> > >&walls){
	for(int y=0;y<walls.size();y++)
		for(int x=0;x<walls[0].size();x++)
			for(int d=0;d<4;d++)
				if(walls[y][x][d])
					return y;
	return 0;
}
//Decide whether maze sections should be printed:
bool verwall(int x,int y,vector<vector<vector<bool> > >&walls){
	return cwall(x-1,y,0,walls)||cwall(x,y,2,walls);
}
bool horwall(int x,int y,vector<vector<vector<bool> > >&walls){
	return cwall(x,y-1,1,walls)||cwall(x,y,3,walls);
}
int intersect(int x,int y,vector<vector<vector<bool> > >&walls){
	return horwall(x,y,walls)+2*verwall(x,y,walls)+4*horwall(x-1,y,walls)+8*verwall(x,y-1,walls);
}
//Interfaces which allow arrows to be printed among the maze sections:
string midharr(int x,int y,int arrows[2][3],string symb[],string harr[]){
	//There is definitely not a wall here.
	//Should there be an arrow here?
	for(int i=0;i<2;i++)
		if(arrows[i][0]==y&&arrows[i][1]==x)
			return harr[arrows[i][2]+8*i];
	return symb[0];
}
string verharr(int x,int y,vector<vector<vector<bool> > >&walls,int arrows[2][3],string symb[],string harr[]){
	//Only consider printing an arrow if there's not already a wall here:
	int original=verwall(x,y,walls)*2;
	if(original!=0)return symb[original];
	//Should there be an arrow here?
	for(int i=0;i<2;i++){
		//Is there an arrow to the right?
		if(arrows[i][0]==y&&arrows[i][1]==x&&arrows[i][2]==0)
			return harr[4+8*i];
		//Is there an arrow to the left?
		if(arrows[i][0]==y&&arrows[i][1]==x-1&&arrows[i][2]==2)
			return harr[6+8*i];
	}
	return symb[original];
}
string horharr(int x,int y,vector<vector<vector<bool> > >&walls,int arrows[2][3],string symb[],string harr[]){
	//Only consider printing an arrow if there's not already a wall here:
	int original=horwall(x,y,walls);
	if(original!=0)return symb[original];
	//Should there be an arrow here?
	for(int i=0;i<2;i++){
		//Is there an arrow up?
		if(arrows[i][0]==y&&arrows[i][1]==x&&arrows[i][2]==1)
			return harr[5+8*i];
		//Is there an arrow down?
		if(arrows[i][0]==y-1&&arrows[i][1]==x&&arrows[i][2]==3)
			return harr[7+8*i];
	}
	return symb[original];
}
string interharr(int x,int y,vector<vector<vector<bool> > >&walls,string symb[]){
	//This function is pretty useless since there's never an arrow here.
	//I left it in for completion and to make the print code look nicer.
	return symb[intersect(x,y,walls)];
}
//Check the state of a wall without IndexOutOfBounds:
bool cwall(int x,int y,int d,vector<vector<vector<bool> > >&walls){
	if(x>=0&&y>=0&&y<walls.size()&&x<walls[0].size())
		return walls[y][x][d];
	//If it's outside the map it's not a wall.
	return false;
}
//Check whether a space is free without IndexOutOfBounds:
bool c(int x,int y,vector<vector<int> >&map,int searched){
    if(x>=0&&y>=0&&y<map.size()&&x<map[0].size())
        return map[y][x]==searched;
    //If it is outside the map it is clearly not what we're looking for.
    return false;
}
//Check whether a space is < the specified value without IndexOutOfBounds:
bool cb(int x,int y,vector<vector<int> >&map,int searched){
    if(x>=0&&y>=0&&y<map.size()&&x<map[0].size())
        return map[y][x]<searched;
    //If it is outside the map it is clearly not what we're looking for.
    return false;
}
//Check whether a space is valid for shape generation (d is the direction we came from):
bool cs(int x,int y,int d,vector<vector<int> >&map){
    //Invalid if outside the map:
	if(x<0||y<0||y>=map.size()||x>=map[0].size())return false;
	//Invalid if occupied or abandoned:
	if(map[y][x]!=0)return false;
	//Surrounding spaces:
	int sur[5][2]{
			{my(y,ccw(d)),mx(x,ccw(d))},
			{my(my(y,d),ccw(d)),mx(mx(x,d),ccw(d))},
			{my(y,d),mx(x,d)},
			{my(my(y,d),cw(d)),mx(mx(x,d),cw(d))},
			{my(y,cw(d)),mx(x,cw(d))}
	};
	//Invalid if any of these is occupied (it doesn't matter if they're abandoned)(those outside the map don't count):
	for(int i=0;i<5;i++)
		//Within the map?
		if(!(sur[i][0]<0||sur[i][1]<0||sur[i][1]>=map.size()||sur[i][0]>=map[0].size()))
			//Occupied?
			if(map[sur[i][0]][sur[i][1]]>0)return false;
	return true;
}
//Cycle a turing generator:
int cycle(int&value,int&state,vector<vector<vector<int> > >&cards){
	//Choose the return value:
	int nextvalue=cards[state][0][value];
	//Modify the state by following this card's instructions:
	state=cards[state][1][value];
	//Modify the value by following this card's instructions:
	value=nextvalue;
	return value;
}
//Operations over coordinate variables:
//Move 1 space in the given direction the from given coordinates:
int mx(int x,int d){
    if(d==0)return x+1;
    if(d==2)return x-1;
    return x;
}
int my(int y,int d){
    if(d==1)return y+1;
    if(d==3)return y-1;
    return y;
}
int mx(int x,int d,int n){
    if(d==0)return x+n;
    if(d==2)return x-n;
    return x;
}
int my(int y,int d,int n){
    if(d==1)return y+n;
    if(d==3)return y-n;
    return y;
}
//These are pass-by-reference to make certain things faster:
void mxx(int&x,int d){
    if(d==0)x++;
    else if(d==2)x--;
}
void myy(int&y,int d){
    if(d==1)y++;
    else if(d==3)y--;
}
void mxy(int&x,int&y,int d){
    if(d==0)x++;
    else if(d==1)y++;
    else if(d==2)x--;
    else y--;
}
int cornx(int d,int MW){
	if(d==1||d==2)return 0;
	else return MW;
}
int corny(int d,int MH){
	if(d>1)return 0;
	else return MH;
}
//Operations over direction variables:
int ccw(int d){
	if(d==3)return 0;
	return d+1;
}
int cw(int d){
	if(d==0)return 3;
	return d-1;
}
int rvs(int d){
	if(d>1)return d-2;
	return d+2;
}
void ccw_(int&d){
	if(++d==4)d=0;
}
void cw_(int&d){
	if(--d==-1)d=3;
}
void rvs_(int&d){
	d+=2;
	if(d>3)d-=4;
}
//Operations over edge variables:
int edgx(int e,int MH,int MW){
	//Bottom edge:
	if(e<MW)return e;
	//Right edge:
	if(e<MW+MH)return MW-1;
	//Top edge:
	if(e<2*MW+MH)return 2*MW+MH-e-1;
	//Left edge:
	return 0;
}
int edgy(int e,int MH,int MW){
	//Bottom edge:
	if(e<MW)return 0;
	//Right edge:
	if(e<MW+MH)return e-MW;
	//Top edge:
	if(e<2*MW+MH)return MH-1;
	//Left edge:
	return 2*MW+2*MH-e-1;
}
int edgd(int e,int MH,int MW){
	//Bottom edge:
	if(e<MW)return 1;
	//Right edge:
	if(e<MW+MH)return 2;
	//Top edge:
	if(e<2*MW+MH)return 3;
	//Left edge:
	return 0;
}
int edgccw(int e,int MH,int MW){
	if(e==2*MH+2*MW-1)return 0;
	return e+1;
}
int edgcw(int e,int MH,int MW){
	if(e==0)return 2*MH+2*MW-1;
	return e-1;
}
//Conversion from any string to a usable seed:
int convert(string x){
	//Check if it's a number:
	bool nan=false;
	for(int i=0;i<x.length();i++)
		if(!isdigit(x.at(i)))
			nan=true;
	if(nan)return myhash(x);
	stringstream cnv;
	cnv<<x;
	int out;
	cnv>>out;
	return out;
}
int myhash(string x){
	int code=0;
	for(int i=0;i<x.length();i++)
		code=code*31+x.at(i);
	return code;
}

/*<cards manual>

Each generator has a list of cards.

The variable CARDS determines how long these lists are.

Each card consists of a list of "set" values and "next" values.

Dimensions:
cards[4][CARDS][2][3 (though only 2 are used at certain generators)]

Dimension meaning:
cards
[which generator? 0=shape_double 1=shape_triple 2=path_double 3=path_triple]
[which card (this is determined by the current "state" value)]
[0=set 1=next]
[which value (this is determined by the current "value" value)]

</cards manual>*/

/*<STANDARDS>

x = hor
y = ver

map[ver][hor]
map[y][x]
walls[y][x][direction]

value meaning:

	map[][] during shape generation:
		0 = not visited
		[1,+inf] = border
		-1 = abandoned

	map[][] during path generation:
		0 = not visited
		1 = visited
		2 = fin
		4 = border

	walls[][][]:
		false = hole or outside the maze
		true = wall

directions and wall sides:

	0 = +x = right
	1 = +y = up
	2 = -x = left
	3 = -y = down

edge:

	0 is at y=0 x=0
	ascending is ccw along the edge
	maximum is 2*MH+2*MW-1


</STANDARDS>*/
