all: smgx;

smgx: smgx.cpp
	g++ -O3 -march=native smgx.cpp -o smgx

clean:
	rm -f smgx
